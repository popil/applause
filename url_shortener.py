import itertools
import string
import sys
from datetime import datetime

from tabulate import tabulate
from tinydb import TinyDB, Query
from tinydb import where
from tinydb.operations import increment

__author__ = 'Oleksandr Popil'
__version__ = '0.1'

db = TinyDB('db.json')
query = Query()

ROOT_URL = 'applau.se/'
DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'


WELCOME_TEMPLATE = """
Please type your choice number and press Enter:
[1] shorten URL
[2] retrieve URL
[3] admin data
[0] exit
"""


SHORTENED_URL = 'SHORTENED URL'
ORIGINAL_URL = 'ORIGINAL URL'
CALL_COUNT = 'CALL COUNT'
LATEST_CALL = 'LATEST CALL'
HEADER = [SHORTENED_URL, ORIGINAL_URL, CALL_COUNT, LATEST_CALL]


def get_all_combos():
    lower_a = string.ascii_lowercase
    upper_a = string.ascii_uppercase
    numbers = string.digits
    allowed_chars = lower_a + upper_a + numbers
    all_combos = []
    for r in range(1, 3):
        for s in itertools.product(allowed_chars, repeat=r):
            combo = ''.join(s)
            all_combos.append(combo)
    return all_combos


def get_available_combo():
    all_combos = get_all_combos()
    used_combos = {}
    for row in get_all_records():
        timestamp = datetime.strptime(row[LATEST_CALL], DATE_FORMAT)
        used_combos[timestamp] = row[SHORTENED_URL]

    # if all combos are in use, pick the oldest one
    if len(all_combos) == len(used_combos.values()):
        oldest_call = min(used_combos.keys())
        return used_combos[oldest_call]

    else:
        for combo in all_combos:
            if combo not in used_combos.values():
                return combo


def get_all_records():
    all_records = [row for row in db.search(query)]
    return all_records


def shorten_url():
    input_url = input('Type URL to be shortened: ')
    record = db.search((where(ORIGINAL_URL) == input_url))
    time_now = datetime.now().strftime(DATE_FORMAT)

    if record:
        # update latest call and increment call count
        db.update({LATEST_CALL: time_now}, (where(ORIGINAL_URL) == input_url))
        db.update(increment(CALL_COUNT), (where(ORIGINAL_URL) == input_url))
        print(ROOT_URL + record[0][SHORTENED_URL])

    else:
        # otherwise create new record
        combo = get_available_combo()
        db.upsert({SHORTENED_URL: combo, ORIGINAL_URL: input_url, CALL_COUNT: 1, LATEST_CALL: time_now},
                  where(SHORTENED_URL) == combo)
        print(ROOT_URL + combo)


def retrieve_url():
    # validate URL path combo
    valid_combos = get_all_combos()
    combo = input('Type shortened URL path: ' + ROOT_URL)
    if combo not in valid_combos:
        sys.exit('URL path must have 3-character length and contain only letters and/or numbers!')

    record = db.search((where(SHORTENED_URL) == combo))
    if record:
        print('Original URL:', record[0][ORIGINAL_URL])
    else:
        print(ROOT_URL + combo, 'does not exist!')


def output_admin_data():
    all_records = get_all_records()
    # sort records by call count in descending order
    ordered_records = sorted(all_records, key=lambda r: r[CALL_COUNT], reverse=True)
    table_lines = []
    for row in ordered_records:
        row[SHORTENED_URL] = ROOT_URL + row[SHORTENED_URL]
        values = list(row.values())
        table_lines.append(values)
    print(tabulate(table_lines, headers=HEADER))


print(WELCOME_TEMPLATE)
choice_number = input()
choice_mapper = {
    '1': shorten_url,
    '2': retrieve_url,
    '3': output_admin_data,
    '0': sys.exit
}

if choice_number in choice_mapper:
    choice_mapper[choice_number]()
else:
    print('Choice does not exist!')
