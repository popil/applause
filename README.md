## URL Shortener
#### Initial Requirements:
* Shorten a URL
    * Input: A regular URL (not from applau.se domain)
    * Output: A shortened URL (use only ten digits, 26 lowercase characters, 26 uppercase characters) of extra length 2 from a given link (example: applau.se/5s)
    * Handle the case that the 2-character length is running out of choices by retiring the shortened URL that has not been called for the longest time

* Retrieve a URL
    * Input: A shortened URL (from applau.se domain)
    * Output: Retrieve the original URL

* Basic admin
    * Show all stored shortened URLs (including shortened URL, original URL, call count and latest call time) and sort by call count

#### Features:
  - shorten URL
  - retrieve original URL by shortened one
  - table with stored URL data

#### Suggested Improvements:
  - add URL validator
  - add table sorting by any column
  - add table record removal 
  - store only shortened URL path without ```applau.se/``` (implemented)
  
#### Prerequisites:
  - Python v3.6+
  - pip package manager
  
#### Getting Started:
  - clone the project
  - ```cd applause```
  - install required packages ```pip install -r requirements.txt```
  - run the script ```url_shortener.py``` or ```python url_shortener.py``` (depending on your configuration)
  - follow commandline instructions
